const week = [
  {
    id: 1,
    value: 'Monday',
  }, {
    id: 2,
    value: 'Tuesday',
  }, {
    id: 3,
    value: 'Wednesday',
  }, {
    id: 4,
    value: 'Thursday',
  }, {
    id: 5,
    value: 'Friday',
  },
];

export default week;
