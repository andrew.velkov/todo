const status = [
  {
    id: 1,
    text: 'New',
    value: 'new',
  }, {
    id: 2,
    text: 'In Progress',
    value: 'progress',
  }, {
    id: 3,
    text: 'Done',
    value: 'done',
  },
];

export default status;
