import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';
import { debounce } from 'lodash';

import { getTodo, createTodo } from 'redux/reducers/todo';
import Fetching from 'components/Fetching';
import Input from 'components/Form/Input';
import ItemAdd from 'components/TodoItems/Add';
import ItemEdit from 'components/TodoItems/Edit';
import Preview from 'components/TodoItems/Preview';

import week from 'config/week';
import statusConfig from 'config/status';

import css from 'style/pages/Todo';

@connect(state => ({
  todo: state.todo.get,
}),
{ getTodo, createTodo }
)
export default class Todo extends Component {
  static propTypes = {
    todo: PropTypes.object,
    getTodo: PropTypes.func,
    createTodo: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      title: '',
      text: '',
      date: Date.now(),
      status: 'new',
      search: '',
      item: {},
      isOpen: null,
    };
  }

  componentDidMount() {
    this.props.getTodo();
  }

  componentWillUnmount() {
    this.debounceEvent = false;
  }

  debounceEvent = (...args) => {
    const debounceEvent = debounce(...args);

    return (event) => {
      event.persist();
      return debounceEvent(event);
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleChangeSearch = (e) => {
    this.props.getTodo(e.target.value);
  }

  handleSubmit = async (e, week) => {
    e.preventDefault();
    const { title, text, date, status } = this.state;
    const data = { title, text, date, status, week };

    if (this.state.title !== '') {
      await this.props.createTodo(data);
      this.props.getTodo();
    }

    this.setState({
      title: '',
      text: '',
      isOpen: false,
    });
  }

  preview = (item) => {
    this.setState({ item });
  };

  render() {
    const { data, loading } = this.props.todo;
    const { search, title, text, status, item, isOpen } = this.state;
    const statusText = statusConfig.find(status => status.value === item.status);

    return (
      <section className={ css.wrap }>
        <div className={ css.wrap__content }>
          <div className={ css.wrap__search }>
            <Input
              label='Search...'
              name='search'
              onChange={ this.debounceEvent(this.handleChangeSearch, 500) }
            />
          </div>

          <Fetching isFetching={ loading } size={ 42 } thickness={ 7 }>
            <ul className={ css.list }>
              {week.map(week => {
                return (
                  <li key={ week.id } className={ css.list__item }>
                    <h2 className={ css.list__header }>{ week.value }</h2>
                    <div className={ css.todoList }>

                      <div className={ css.todoList__wrap }>
                        {data.length >= 1 && data.filter(item => item.week === week.value).map((todo, index) => {
                          return (
                            <ItemEdit
                              key={ todo.id }
                              todo={ todo }
                              index={ index }
                              status={ status }
                              week={ week.value }
                              onClick={ () => this.preview(todo) }
                            />
                          );
                        })}
                      </div>

                      <ItemAdd
                        title={ title }
                        text={ text }
                        week={ week.value }
                        isOpen={ isOpen }
                        handleChange={ this.handleChange }
                        onClick={ (e) => this.handleSubmit(e, week.value) }
                      />

                    </div>
                  </li>
                );
              })}
            </ul>
          </Fetching>
        </div>

        {(item.id && !search) && <Preview className={ cx(css.wrap__content, css.wrap__content_offset) }item={ item } statusText={ statusText.text } />}
      </section>
    );
  }
}
