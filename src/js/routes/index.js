import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from 'routes/history';
import App from 'containers/App';

import Todo from 'pages/Todo';
import NotFound from 'pages/NotFound';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <App>
          <Switch>
            <Route exact path='/' render={ (props) => <Todo { ...props } /> } />
            <Route component={ NotFound } />
          </Switch>
        </App>
      </Router>
    );
  }
}
