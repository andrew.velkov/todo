import { createStore, applyMiddleware, compose } from 'redux';
// import logger from 'redux-logger';

import reducer from './reducers';
import apiMiddleWare from './middleware/apiMiddleware';

const initialState = {};

const store = createStore(
  reducer,
  initialState,
  compose(
    applyMiddleware(apiMiddleWare),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

export default store;
