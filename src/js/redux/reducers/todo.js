const GET_TODO = 'todo/GET_TODO';
const GET_TODO_SUCCESS = 'todo/GET_TODO_SUCCESS';
const GET_TODO_ERROR = 'todo/GET_TODO_ERROR';

const CREATE_TODO = 'todo/CREATE_TODO';
const CREATE_TODO_SUCCESS = 'todo/CREATE_TODO_SUCCESS';
const CREATE_TODO_ERROR = 'todo/CREATE_TODO_ERROR';

const EDIT_TODO = 'todo/EDIT_TODO';
const EDIT_TODO_SUCCESS = 'todo/EDIT_TODO_SUCCESS';
const EDIT_TODO_ERROR = 'todo/EDIT_TODO_ERROR';

const REMOVE_TODO = 'todo/REMOVE_TODO';
const REMOVE_TODO_SUCCESS = 'todo/REMOVE_TODO_SUCCESS';
const REMOVE_TODO_ERROR = 'todo/REMOVE_TODO_ERROR';

const initialState = {
  get: {
    loaded: false,
    loading: false,
    data: [],
  },
  create: {
    loaded: false,
    loading: false,
    status: '',
  },
  edit: {
    loaded: false,
    loading: false,
    status: '',
  },
  remove: {
    loaded: false,
    loading: false,
    status: '',
  },
};

export default function postReducer(state = initialState, action) {
  switch (action.type) {
    case GET_TODO:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case GET_TODO_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          data: action.payload.result,
        },
      };
    case GET_TODO_ERROR:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: false,
        },
      };
    case CREATE_TODO:
      return {
        ...state,
        create: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case CREATE_TODO_SUCCESS:
      return {
        ...state,
        create: {
          loading: false,
          loaded: true,
          status: 'Created!',
        },
      };
    case CREATE_TODO_ERROR:
      return {
        ...state,
        create: {
          loading: false,
          status: action.payload,
        },
      };
    case EDIT_TODO:
      return {
        ...state,
        edit: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case EDIT_TODO_SUCCESS:
      return {
        ...state,
        edit: {
          loading: false,
          loaded: true,
          status: 'Success',
        },
      };
    case EDIT_TODO_ERROR:
      return {
        ...state,
        edit: {
          loading: false,
          status: action.payload,
        },
      };
    case REMOVE_TODO:
      return {
        ...state,
        remove: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case REMOVE_TODO_SUCCESS:
      return {
        ...state,
        remove: {
          loading: false,
          loaded: true,
          status: 'Successfully deleted!',
        },
      };
    case REMOVE_TODO_ERROR:
      return {
        ...state,
        remove: {
          loading: false,
          status: action.payload,
        },
      };
    default:
      return state;
  }
}

export const getTodo = (value = '') => {
  return {
    types: [GET_TODO, GET_TODO_SUCCESS, GET_TODO_ERROR],
    request: {
      method: 'GET',
      url: `/api/todo?q=${ value }`,
    },
  };
};

export const createTodo = data => {
  return {
    types: [CREATE_TODO, CREATE_TODO_SUCCESS, CREATE_TODO_ERROR],
    request: {
      method: 'POST',
      url: '/api/todo',
      body: data,
    },
    status: 'Created!',
  };
};

export const editTodo = (id, data) => {
  return {
    types: [EDIT_TODO, EDIT_TODO_SUCCESS, EDIT_TODO_ERROR],
    request: {
      method: 'PUT',
      url: `/api/todo/${ id }`,
      body: data,
    },
    status: 'Edited Succes!',
  };
};

export const removeTodo = id => {
  return {
    types: [REMOVE_TODO, REMOVE_TODO_SUCCESS, REMOVE_TODO_ERROR],
    request: {
      method: 'DELETE',
      url: `/api/todo/${ id }`,
    },
    status: 'Removed!',
  };
};
