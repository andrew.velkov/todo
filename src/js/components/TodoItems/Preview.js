import React from 'react';
import PropTypes from 'prop-types';

const Preview = ({ item, modal, className, statusText }) => {
  const text = item.text.split('\n').map((item) => <span key={ item.id }>{ item }<br /></span>);

  return (
    <div className={ className }>
      {!modal && <h3>{ item.title }</h3>}
      <hr />
      <p><strong>Date:</strong> { item.week }, { item.date }</p>
      <p><strong>Status:</strong> { statusText }</p>
      <hr />
      <p>{ text }</p>
    </div>
  );
};

Preview.propTypes = {
  item: PropTypes.object,
  statusText: PropTypes.string,
  className: PropTypes.string,
  modal: PropTypes.bool,
};

export default Preview;
