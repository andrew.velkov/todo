import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Dialog from 'components/Dialog';
import Input from 'components/Form/Input';

import css from 'style/pages/Todo';

const ItemAdd = ({ title, isOpen, text, week, handleChange, onClick }) => (
  <div className={ cx(css.todoList__item, css.todoList__item_add) } title='Add Todo'>
    <Dialog
      title={ `Add task / ${ week }` }
      buttonName='Add task'
      // primaryClass={ true }
      disabled={ title === '' }
      isOpen={ isOpen }
      onClick={ onClick }
    >
      <form className={ css.form }>
        <ul className={ css.form__list }>
          <li className={ css.form__item }>
            <Input name='title' label='Title *' value={ title } onChange={ handleChange } />
          </li>
          <li className={ css.form__item }>
            <Input name='text' label='Description' value={ text } onChange={ handleChange } multiLine={ true } />
          </li>
        </ul>
      </form>
    </Dialog>
  </div>
);

ItemAdd.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  week: PropTypes.string,
  handleChange: PropTypes.func,
  isOpen: PropTypes.any,
  onClick: PropTypes.func,
};

export default ItemAdd;
