import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getTodo, editTodo, removeTodo } from 'redux/reducers/todo';
import Dialog from 'components/Dialog';
import Popover from 'components/Popover';
import Select from 'components/Form/Select';
import Input from 'components/Form/Input';
import Preview from 'components/TodoItems/Preview';

import statusConfig from 'config/status';

import css from 'style/pages/Todo';

@connect(() => ({}), { getTodo, editTodo, removeTodo })
export default class ItemEdit extends Component {
  static propTypes = {
    todo: PropTypes.any,
    index: PropTypes.number,
    week: PropTypes.string,
    getTodo: PropTypes.func,
    editTodo: PropTypes.func,
    onClick: PropTypes.func,
    removeTodo: PropTypes.func,
  };

  constructor(props) {
    super(props);
    const { title, text, status, date } = this.props.todo;
    this.state = { title, text, status, date, isOpen: null };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleChangeSelect = (e, index, data) => {
    this.setState({
      status: data.value,
    });
  }

  handleEditTodo = async (e, week, id) => {
    e.preventDefault();
    const data = {
      title: this.state.title,
      text: this.state.text,
      status: this.state.status,
      date: this.state.date,
      week,
    };
    if (this.state.title !== '') {
      await this.props.editTodo(id, data);
      this.props.getTodo();
      this.setState({
        isOpen: false,
      });
    }
  }

  handleRemoveTodo = async (e, id) => {
    e.preventDefault();
    await this.props.removeTodo(id);
    this.props.getTodo();
    this.setState({
      isOpen: false,
    });
  }

  render() {
    const { todo, index, week, onClick } = this.props;
    const { title, text, status, isOpen } = this.state;
    const statusText = statusConfig.find(status => status.value === todo.status);

    return (
      <div className={ cx(css.todoList__item, css[`todoList__item_${ todo.status }`]) } title={ todo.title }>
        <Dialog
          className={ css.todoList__title }
          title={ `Edit todo / ${ week }` }
          buttonType='link'
          disabled={ title === '' }
          buttonName={ `${ index + 1 }. ${ todo.title }` }
          preview={ onClick }
          isOpen={ isOpen }
          onClick={ (e) => this.handleEditTodo(e, week, todo.id) }
        >
          <form className={ css.form }>
            <ul className={ css.form__list }>
              <li className={ css.form__item }>
                <Input name='title' label='Title *' value={ title } onChange={ this.handleChange } />
              </li>
              <li className={ css.form__item }>
                <Select data={ statusConfig } label='Status' value={ status } handleChange={ this.handleChangeSelect } />
              </li>
              <li className={ css.form__item }>
                <Input name='text' label='Description' value={ text } onChange={ this.handleChange } multiLine={ true } />
              </li>
            </ul>
          </form>
        </Dialog>

        <div className={ css.todoList__actions }>
          <Dialog
            className={ css.todoList__title }
            title={ todo.title }
            buttonType='icon'
            buttonName='visibility'
            buttonSend={ false }
          >
            <Preview item={ todo } modal={ true } statusText={ statusText.text } />
          </Dialog>

          <Popover iconName='delete' onClick={ (e) => this.handleRemoveTodo(e, todo.id) } />
        </div>
      </div>
    );
  }
}
