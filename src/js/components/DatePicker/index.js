import React from 'react';
import PropTypes from 'prop-types';
import DatePickerMaterial from 'material-ui/DatePicker';

const DatePicker = ({ text, value }) => (
  <DatePickerMaterial hintText={ text } value={ value } />
);

DatePicker.propTypes = {
  text: PropTypes.string,
  value: PropTypes.any,
};

export default DatePicker;
