# todo-list

![alt text](http://i.piccy.info/i9/0f41ab2de3e59f3ff6e5197ff26f0ba5/1534467496/55466/1260166/Screenshot_1.png)
![alt text](http://i.piccy.info/i9/d88f88b0c716ed6fe77b5bbe3dd6ca85/1534467510/85711/1260166/Screenshot_2.png)

```
$ git clone https://gitlab.com/andrew.velkov/todo.git
```

```
$ cd todo
```

```
$ yarn install
```

For start server with mockups:
```
$ yarn start:mock
```

Visit `http://localhost:3003/` 