const child_process = require('child_process');

const isWin = /^win/.test(process.platform);

if (isWin) {
  child_process.execSync('start node mockups/server');
} else {
  child_process.execSync('node mockups/server');
}
